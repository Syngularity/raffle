package org.qian.raffle;

import java.util.Scanner;

/**
 * Created by charlesqian on 12/7/15.
 */
public class RaffleApp {

    public static void main(String[] args) {

        //asks for chance in decimal
        Raffle raffle = new Raffle();
        Scanner input = new Scanner(System.in);
        System.out.println("How many winning tickets?:");
        int wincount=0;
        wincount = input.nextInt();
        raffle.setWinCount(wincount);

        System.out.println("Win chance (eg 0.50):");

        raffle.setChance(input.nextDouble());

        raffle.seed(); //Let's populate the winners array with the winning numbers



        System.out.println("How many times should we simulate drawing?:");
        raffle.draw(input.nextInt()); //Draw X number of times...

    }

}
