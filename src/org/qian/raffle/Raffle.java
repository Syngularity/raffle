package org.qian.raffle;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by charlesqian on 12/7/15.
 */
public class Raffle {
    private int drawCount,winCount,winnersLeft,max,won=0,winningNumber;
    private double chance,confidence=0.85;
    private int[] winners = {};
    private int[] alreadyWon = {};

    //Setter for chance variable.
    public void setChance(double chance){
        this.chance = chance;
        int projectedrolls;
        projectedrolls=((int)(Math.log(1-this.confidence)/Math.log(1-(double)this.chance)))*winCount;
        System.out.println("Raffle chance has been set to: " + this.chance +" ["+this.chance*100+"%] with a projected roll count of " +projectedrolls);
    }
    //Setter for how many wins.
    public void setWinCount(int count){
        this.winCount = count;
        this.winners = new int[count]; //going to define the size of the array of winners since we now know
        this.alreadyWon = new int[count];
        this.winnersLeft = count;
        System.out.println("There will be " + this.winCount + " winning numbers.");
    }

    public int getMax(){
        return this.max;
    }

    /**Picks all the winning numbers ahead of time based on determined win %. Creates a [1 to X] situation where 1/X = win percentage.
     * EXAMPLE: 0.5 chance will create a 1 to 2 situation where 'max' will resolve to 2 and so a random number will be chosen from 1 to max [2]
     * Thus creating the 0.5 scenario. IDK MADE SENSE AT THE TIME. CODE WORKS. FUCK OFF. */
    public void seed(){
        int currentPick;
        //idk about this math, seems to work. Turn our percentage into a 1/X chance where X is the max number to random from. Derp. idk mang...
        max= (int)Math.round(1/this.chance);

        if (this.winCount>this.max) return;

            System.out.println("Seeding winning numbers with chance "+this.chance*100+"%");
            Random rand = new Random();
            currentPick=rand.nextInt(max)+1;

            this.winningNumber = currentPick;
            System.out.println("Picking a number from 1-"+max+": " +this.winningNumber);


    }
    public void draw(int count){
        int currentPick,w=0;
        for(int i=1; i<count+1; i++){
            Random rand = new Random();
            currentPick=rand.nextInt(max)+1;

            //If we picked a winner let's note the attempt number and percentage win since last win
            if (winningNumber==currentPick && winnersLeft>0){
                //Quick check to see if this winning number has already been used before...

                    System.out.println("WIN on attempt #" + i + " with a pick of " + currentPick + " [1-" + max + "]");
                    System.out.println("Projected win %: " + this.chance * 100 + "% Actual rate: " + (1 / ((double) (i - w))) * 100 + "%");
                    w = i; //keep track of number of attempts since last win.
                    winnersLeft--;
                    won++;
                    //Check to see if there's any more winners left.
                    if (winnersLeft==0) {
                        System.out.println("All winners drawn!");
                        break;
                    }


            }
        }
        //We're out of draws but there's still winning numbers left!
        if(winnersLeft>0) {
            System.out.println("Won "+won+" times with "+winnersLeft+" remaining winning seeds");
            System.out.println("How many to redraw?:");
            Scanner input = new Scanner(System.in);
            draw(input.nextInt()); //Draw X number of times...
        }
    }
    //Returns True/False. Checks to see if the current picked number is in the array
    public boolean check(int array[], int element) {
        for (int i = 0; i < array.length; i++) {
            if (element == array[i]) {
                return true;
            }
        }
        return false;
    }
}
